# gitlab reverse proxy
As gitlab migrated to google, there are some issue accessing your repositories in some countries. here's a solution using reverse proxy. it's secure as it's open-source and you can have it on your own servers.

**be aware** that there is no ssh access using this method. it only allows you have the full website access and https access to repository

# requirement
* you need to know basic docker and you need docker and docker-compose installed.

# configurations
## certifications
first you need to have ssl certifications. if you don't have any crt file you can use [sslforfree.com](https://www.sslforfree.com). the _/.well-known/acme-challenge_ directory is for your verification file.

* put your **private.key** data into `cert.key` file
* merge your **certification.crt** and **ca-bundle.crt** file into `cert.crt` as follow
    * first put **certification.crt**
    * then add a new line (press enter key)
    * then paste your bundle to the rest
    * you can check the [existing file as sample](./cert.crt)

## nginx configs
open [proxy.conf](./proxy.conf) file and edit the two places with `[your host domain here]` and replace it with your own domain (without `[   ]`) example `git.mysite.com`.

# run
make sure your 80 port and 443 port has not listened with apache or other nginx and run the server using `docker-compose`. if they are already in use you can change them with the steps below(__change pots__).

```
sudo docker-compose up -d
```

and you can clone / push / pull the repository by replacing `gitlab.com` with your url `git.mysite.com`
```
git clone https://git.mysite.com/repository_name.git
```

# customize
## change ports
you can edit [docker-compose.yml](./docker-compose.yml) file and change the left port number as the output port you want. (these ports will be exposed)

**NOTE**: if you change the 443 port your git commands will need the ports be assigned

```yml
    ...
    ports:
      - 443:443
      - 80:80

```
example i want to have it on port 8443 and 8080  
```yml
    ...
    ports:
      - 8443:443
      - 8080:80

```
and my clone request will be 
```
git clone https://git.mysite.com:8443/repository_name.git
```
## disable port 80
there's no need of port _80_ after the certification registration. you can disable it as below.
```yml
    ...
    ports:
      - 443:443
      #- 80:80

```